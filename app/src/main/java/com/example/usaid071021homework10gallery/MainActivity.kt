package com.example.usaid071021homework10gallery

import android.content.ClipData
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import com.example.usaid071021homework10gallery.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var adapter: PhotosAdapter

    private lateinit var adapter2: PhotosAdapter

    val items = mutableListOf<Items>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.recyclerView.layoutManager = GridLayoutManager(this, 3)
        adapter = PhotosAdapter(setData())
        binding.recyclerView.adapter = adapter
        searchData()
    }

    private fun setData(): MutableList<Items> {
        items.add(Items(R.mipmap.cxeli1, "ოპერაცია - პაუკა", "გენო შავდია - 'დაყარეთ იარაღი და გამოდით, ნიჟარაძე უკან'"))
        items.add(Items(R.mipmap.cxeli2, "სასაფლაოს მოჩვენება" , "გლობუსა კალდუნას უეცარმა გამოჩენამ დააფრთხო"))
        items.add(Items(R.mipmap.cxeli3, "ბუტკის პატრონი" , "ბონდოს გატისკვამდე დარჩენილია 2 წუთი"))
        items.add(Items(R.mipmap.cxeli4, "სტუმრად ომარასთან", "ზახარიჩი ჩრდილში უფრო ლამაზია"))
        items.add(Items(R.mipmap.cxeli5, "ბორის ჯოტოს ძე გარუჩავა", "'არც არაფერს ვადასტურებ და არც არაფერს ვუარყოფ'"))
        items.add(Items(R.mipmap.cxeli6, "გამომძიებელი ედემი ღურწკაია, რაჟდენიჩი", "ნამდვილი გამომძიებელი სიმართლის დასადგენად ყველაფერს იკადრებს"))
        items.add(Items(R.mipmap.cxeli7, "გურამა ჯინორია", "-რუსეთი საქართველოს გარეშე ვერ იარსებებს "))
        return items
    }

    private fun searchData() {
        binding.searchBar.addTextChangedListener{text ->
            if (text.toString() == "") {
                (binding.recyclerView.adapter as PhotosAdapter).setData(items)
            }else {
                    val item = items.filter { it.name.contains(text.toString())} as MutableList<Items>
                (binding.recyclerView.adapter as PhotosAdapter).setData(item)
            }
        }


    }


}