package com.example.usaid071021homework10gallery

import java.io.Serializable

class Items(val image: Int, val name: String, val description: String): Serializable {
}