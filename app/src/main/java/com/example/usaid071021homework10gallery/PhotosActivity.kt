package com.example.usaid071021homework10gallery

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.usaid071021homework10gallery.databinding.ActivityPhotosBinding

class PhotosActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPhotosBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPhotosBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        val item = (intent.getSerializableExtra("photoItem")) as Items
        binding.imageView.setImageResource(item.image)
        binding.textviewimagename.setText(item.name)
        binding.textviewdescription.setText(item.description)

    }

}