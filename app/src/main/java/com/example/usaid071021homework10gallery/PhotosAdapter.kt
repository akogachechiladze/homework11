package com.example.usaid071021homework10gallery

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.usaid071021homework10gallery.databinding.ItemLayoutBinding

class PhotosAdapter(private var items: MutableList<Items>): RecyclerView.Adapter<PhotosAdapter.ViewHolder> (){


    inner class ViewHolder (val binding: ItemLayoutBinding) :RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosAdapter.ViewHolder {
        return ViewHolder(ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: PhotosAdapter.ViewHolder, position: Int) {
        holder.binding.imageviewItemLayout.setImageResource(items[position].image)

        holder.binding.imageviewItemLayout.setOnClickListener{
            val intent = Intent(holder.itemView.context, PhotosActivity::class.java)
            intent.putExtra("photoItem", items[position])
            holder.itemView.context.startActivity(intent)
        }


    }

    override fun getItemCount()= items.size


    fun setData(data: MutableList<Items>) {
        this.items = data
        notifyDataSetChanged()
    }
}